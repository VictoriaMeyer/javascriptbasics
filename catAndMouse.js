/*
### Cat and Mouse
  
You will be given a string featuring a cat 'C' and a mouse 'm'. The rest of the string will be made up of '.'.
You need to find out if the cat can catch the mouse from it's current position. The cat can jump three characters. So:
*/

// ES5
function isCaught(sequence) {
    var jumpLength = 3;
    for (var i = 0; i < sequence.length; i++) {
        var chr = sequence[i];
        if (chr === 'C') {
            for (var j = i + 1; j <= i + jumpLength + 1; j++) {
                var chr2 = sequence[j];
                if (chr2 === 'm') {
                    return true;
                }
            }
        }
    }
    return false;
}

const isCaughtES6 = sequence => {
    const jumpLength = 3;
    for (let i = 0; i < sequence.length; i++) {
        let chr = sequence[i];
        if (chr === 'C') {
            for (let j = i + 1; j <= i + jumpLength; j++) {
                let chr2 = sequence[j];
                if (chr2 === 'm') return true;
            }
        }
    }
    return false;
}

console.log(isCaught('C...m')); // => true
console.log(isCaught('..C..m')); // => true
console.log(isCaught('..C...m')); // => true
console.log(isCaught('C.....m')); // => false
console.log(isCaught('C.....m...')); // => false

/*

You need to create a program that will translate from English to Pig Latin.
Pig Latin takes the first consonant (or consonant cluster) of an English word, moves it to the end of the word and suffixes an “ay”. 
If a word begins with a vowel you just add “way” to the end. 
It might not be obvious but you need to remove all the consonants up to the first vowel in case the word does not start with a vowel.

*/

function translatePigLatin(str) {
    const vowel = "aeoui";
    const cluster = str.substr(0, 2);
    if (vowel.includes(cluster[0])) {
        return str + "way";
    } else if (!vowel.includes(cluster[0]) && !vowel.includes(cluster[1])) {
        const end = cluster + "ay";
        return (str = str.substr(2) + end);
    } else {
        const end = str.substr(0, 1) + "ay";
        return (str = str.substr(1) + end);
    }
}

console.log(translatePigLatin("glove"));
console.log(translatePigLatin("pig")); // igpay